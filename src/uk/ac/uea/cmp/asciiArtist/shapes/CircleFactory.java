package uk.ac.uea.cmp.asciiArtist.shapes;

public class CircleFactory extends ShapeFactory {

	@Override
	public Shape Create(int r) {
		return new Circle(r);
	}

	static private ShapeFactory instance = null;
	static public ShapeFactory getInstance()
	{
		if (instance == null)
			return new CircleFactory();
		return instance;
	}
	
	private CircleFactory() { }

}
