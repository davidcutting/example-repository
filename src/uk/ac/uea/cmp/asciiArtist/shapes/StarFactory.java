package uk.ac.uea.cmp.asciiArtist.shapes;

public class StarFactory extends ShapeFactory {

	@Override
	public Shape Create(int r) {
		return new Star(r);
	}

	static private ShapeFactory instance = null;
	static public ShapeFactory getInstance()
	{
		if (instance == null)
			return new StarFactory();
		return instance;
	}
	
	private StarFactory() { }

}
