package uk.ac.uea.cmp.asciiArtist.shapes;

import uk.ac.uea.cmp.asciiArtist.grid.Grid;

public class Star extends Shape {
	public Star(int r)
	{
		R = r;
		int x = r*2;
		int y = x;
		
		g = new Grid(x,y);
		
		for (int i=(y-1); i>=0; --i)
		{
			int xp = (int)(i/2);
			try
			{
				g.Set(R-xp, i, 1);
				g.Set(R+xp, i, 1);
				g.Set(i, y-1, 1);
				
				g.Set(R-xp, y-i, 1);
				g.Set(R+xp, y-i, 1);
				g.Set(i, 1, 1);
			}
			catch(Exception e)
			{
				//
			}
		}
	}
}
