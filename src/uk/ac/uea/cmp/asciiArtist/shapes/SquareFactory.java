package uk.ac.uea.cmp.asciiArtist.shapes;

public class SquareFactory extends ShapeFactory {

	@Override
	public Shape Create(int r) {
		return new Square(r);
	}

	static private ShapeFactory instance = null;
	static public ShapeFactory getInstance()
	{
		if (instance == null)
			return new SquareFactory();
		return instance;
	}
	
	private SquareFactory() { }

}
