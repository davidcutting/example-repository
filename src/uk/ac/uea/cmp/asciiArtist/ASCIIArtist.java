package uk.ac.uea.cmp.asciiArtist;

import uk.ac.uea.cmp.asciiArtist.grid.*;
import uk.ac.uea.cmp.asciiArtist.shapes.*;
import uk.ac.uea.cmp.asciiArtist.grid.print.*;

public class ASCIIArtist {

	public static void main(String[] args) throws Exception {
		
		String firstline = Information.productName+" version "+Information.version+" Starting";
		String secondline = "Author: "+Information.author+", contact: "+Information.contact;
		int length = firstline.length()>secondline.length() ? firstline.length() : secondline.length();
		length += 4;
		String rowLine = "";
		String spaceLine = "";
		for(int i=0; i<length; ++i)
		{
			rowLine += "#";
			if (i == 0 || i == (length-1))
				spaceLine += "#";
			else 
				spaceLine += " ";
		}
		firstline = "# "+firstline;
		secondline = "# "+secondline;
		for (int i=0; i<length-1; ++i)
		{
			if (i>firstline.length())
				firstline+=" ";
			if (i>secondline.length())
				secondline+=" ";
		}
		firstline+=" #";
		secondline+=" #";
		
		System.out.println(rowLine);
		System.out.println(spaceLine);
		System.out.println(firstline);
		System.out.println(secondline);
		System.out.println(spaceLine);
		System.out.println(rowLine);
		
		System.out.println("");
		
		Grid g = new Grid(30,30);
		
		
		// Drawing Elements
		CircleFactory.getInstance().Create(10).Draw(g, 15, 15);
		CircleFactory.getInstance().Create(10).Draw(g, 0, 0);
		CircleFactory.getInstance().Create(10).Draw(g, 10, 10);
		
		//TriangleFactory.getInstance().Create(5).Draw(g, 25, 25);
		StarFactory.getInstance().Create(5).Draw(g, 25, 25);
		
		SquareFactory.getInstance().Create(3).Draw(g,5,25);
		
		// Printing Elements
		PrintMethod pm = new ScreenPrint();
		pm.setOption("showzero", "no");
		g.setPrintMethod(pm);
		
		g.Print();
		
		g.setPrintMethod(new FilePrint());
		g.getPrintMethod().setOption("filename", "output.txt");
		
		g.Print();
		
		g.setPrintMethod(new CSVPrint());
		g.getPrintMethod().setOption("filename","output.csv");
		
		g.Print();
	}

}
