package uk.ac.uea.cmp.asciiArtist.grid;

import uk.ac.uea.cmp.asciiArtist.grid.print.PrintException;
import uk.ac.uea.cmp.asciiArtist.grid.print.PrintMethod;

public class Grid {
	private int X = 0;
	private int Y = 0;
	private int Size = 0;
	private int[] grid;
	private PrintMethod printMethod = null;
	
	public Grid(int xsize, int ysize)
	{
		X = xsize;
		Y = ysize;
		Size = X*Y;
		grid = new int[Size];
		for (int i=0; i<Size; ++i)
			grid[i]=0;
	}
	
	public void setPrintMethod(PrintMethod method)
	{
		printMethod = method;
	}
	
	public PrintMethod getPrintMethod()
	{
		return printMethod;
	}
	
	public void Print() throws PrintException
	{
		if (printMethod == null)
			throw new PrintException("No print method specified");
		printMethod.Print(this);
	}
	
	private int getPos(int x, int y) throws GridBoundsException
	{
		if ( x < 0 || x >= X || y < 0 || y >= Y )
			throw new GridBoundsException("Out of bounds");
		return (y*X)+x;
	}
	
	public int Get(int x, int y) throws GridBoundsException
	{
		return grid[getPos(x,y)];
	}
	
	public void Set(int x, int y, int z) throws GridBoundsException
	{
		grid[getPos(x,y)]=z;
	}
	
	public void Add(int x, int y, int z) throws GridBoundsException
	{
		grid[getPos(x,y)]+=z;
	}
	
	public void Add(int x, int y) throws GridBoundsException
	{
		Add(x,y,1);
	}
	
	protected int getLeftOffset()
	{
		return 0 - (int)(X/2);
	}
	
	protected int getTopOffset()
	{
		return 0 - (int)(Y/2);
	}
	
	public void Overlay(Grid g, int cx, int cy)
	{
		int left = cx + g.getLeftOffset();
		int top = cy + g.getTopOffset();
		for (int y=0; y<g.Y; ++y)
		{
			for (int x=0; x<g.X; ++x)
			{
				try
				{
					Add(left+x, top+y, g.Get(x, y));
				}
				catch(GridBoundsException e)
				{
					// do nothing - was an overlap on the overlaid grid, carry on!
				}
			}
		}
	}
	
	public int getX()
	{
		return X;
	}
	
	public int getY()
	{
		return Y;
	}
}
