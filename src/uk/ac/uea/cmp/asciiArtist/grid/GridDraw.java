package uk.ac.uea.cmp.asciiArtist.grid;

public interface GridDraw {
	public void Draw(Grid canvas, int xpos, int ypos);
}
