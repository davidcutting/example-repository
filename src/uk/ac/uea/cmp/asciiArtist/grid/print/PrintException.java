package uk.ac.uea.cmp.asciiArtist.grid.print;

public class PrintException extends Exception {

	public PrintException(String m)
	{
		super(m);
	}
	
}
