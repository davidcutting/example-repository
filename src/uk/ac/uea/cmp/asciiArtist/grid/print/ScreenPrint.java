package uk.ac.uea.cmp.asciiArtist.grid.print;

import uk.ac.uea.cmp.asciiArtist.grid.Grid;

public class ScreenPrint extends PrintMethod {

	@Override
	public void Print(Grid g) throws PrintException {
		int x = g.getX();
		int y = g.getY();
		boolean blankZero = false;
		
		String zeroOption = getOption("showzero");
		if (zeroOption != null && zeroOption.equalsIgnoreCase("no"))
			blankZero=true;
		
		try
		{
			for (int yp = 0; yp < y; ++yp)
			{
				for (int xp = 0; xp < x; ++xp)
				{
					int n = g.Get(xp,yp);
					String o = ""+n;
					if (blankZero && n == 0)
						o=" ";
					System.out.print(o);
				}
				System.out.println("");
			}
		}
		catch(Exception e)
		{
			throw new PrintException("Exception Occured During Grid: "+e.getMessage());
		}
	}

}
